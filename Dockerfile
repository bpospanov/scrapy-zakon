FROM ubuntu
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

COPY ./scrapy_zakon /scrapy_zakon
VOLUME /scrapy_zakon
VOLUME /csv_output

RUN apt-get update && apt-get install -y python3.6 && apt-get install -y python3-pip
RUN python3.6 -m pip install virtualenv
RUN virtualenv /scrapy_venv && source /scrapy_venv/bin/activate
RUN pip3 install -r /scrapy_zakon/requirements.txt

WORKDIR /scrapy_zakon/zakon
ENTRYPOINT scrapy crawl zakon -o /csv_output/today.csv

