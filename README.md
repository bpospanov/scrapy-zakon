Написать парсер сайта zakon.kz:
1) Парсер, который делает сбор всех сегодняшних новостей по ссылке https://www.zakon.kz/news. Нужно собрать такие данные как: заголовок, полный текст самой статьи, дата публикации, кол-во комментариев. Все собранные поля необходимо сохранить в формате csv.
Требования: 
-Отказоустойчивость парсера (Обработка исключений, ретраи);
-Комментарии к коду
-Возможность использования проксей, обход блокировок по IP;
-Сбор комментариев к статье, если они есть (будет плюсом)
-Файл с использованными зависимостями (библиотеками) - requirements.txt


для работы с комментариями необходимо запустить Splash сервер (Linux):
1. установить Docker версии 17 и выше
2 установить image Splash
    $ sudo docker pull scrapinghub/splash
3.запустить контейнер
    $ sudo docker run -it -p 8050:8050 --rm scrapinghub/splash
4. запуск парсера
4.1 установить virtual environment:
    $ virtualenv scrapy_venv
    $ source scrapy_venv/bin/activate
    $ cd path/to/project/scrapy_zakon/
    $ pip install -r requirements.txt
4.2 запуск spider   
    $ cd path/to/project/scrapy_zakon/zakon
    $ scrapy crawl zakon -o today_news.csv

5.для запуска с прокси:
    $ cd path/to/project/scrapy_zakon/zakon
    обновим список прокси
    $ curl -sSf "https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt" > proxy-list.txt
    раскомментить строки 25 и 26 в settings.py