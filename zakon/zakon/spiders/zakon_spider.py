import scrapy
from ..items import ZakonItem
from scrapy_splash import SplashRequest
from ..settings import my_path, ROTATING_PROXY_LIST_PATH


class ZakonSpider(scrapy.Spider):
    name = 'zakon'
    start_urls = [
        'https://www.zakon.kz/news'
    ]
    url = 'https://www.zakon.kz'

    def parse(self, response):
        news_divs = response.selector.xpath('//div[contains(@class, "cat_news_item")]')  # div со всеми новостями на странице

        date = '9999-99-99'
        date_flag = 0
        for news_div in news_divs:
            if news_div.xpath('span[contains(@class, "date n2")]/text()').get():  # div с классом "date n2" хранит сегодняшнюю дату
                date_flag += 1
                if date_flag > 1:
                    break
                else:
                    date = news_div.xpath('span[contains(@class, "date n2")]/text()').get()
            elif news_div.xpath('span[contains(@class, "date n3")]/text()').get():  # div с сегодняшними новостями имеют класс "date n3"
                yield self.fill_items_from_main_page(news_div, date)
            else:
                yield {'error in div': news_div}

    def fill_items_from_main_page(self, news_div, date):
        items = ZakonItem()
        items['datetime'] = date + ' ' + news_div.xpath('span/text()').get()  # дата публикации
        items['href'] = ZakonSpider.url + news_div.xpath('a/@href').get()  # ссылка на новость
        request = scrapy.Request(items['href'], callback=self.fill_items_from_news_page, cb_kwargs={'items': items})
        return request

    def fill_items_from_news_page(self, response, items):
        if response.selector.xpath('//div[contains(@class, "fullnews white_block")]'):  # первый формат статей
            items['header'] = response.selector.xpath('//h1/text()').get()  # заголовок статьи
            text_list = response.selector.xpath('//div[contains(@id, "initial_news_story")]//p[not(self::p[@id="tg_invite"])]//text() | //div[contains(@id, "initial_news_story")]//*[contains(@class, "quote_in_news")]//text() | //div[contains(@id, "initial_news_story")]//strong//text()').getall()
            items['text'] = ''.join(elem for elem in text_list)  # текст статьи
            items['comment_count'] = response.selector.xpath('//*[contains(@class, "zknc-total-count")]/text()').get()  # количество комментов
        elif response.selector.xpath('//div[contains(@id, "dle-content")]'):  # второй формат статей
            items['header'] = response.selector.xpath('//span[contains(@class, "s1")]/text()').get().replace('\n', ' ').replace('\r', ' ')
            text_list = response.selector.xpath('//div[contains(@class, "WordSection")]//p[contains(@class, "MsoNormal")]/span/text()').getall()
            items['text'] = ''.join(elem.replace('\n', ' ').replace('\r', ' ') for elem in text_list)
            items['comment_count'] = response.selector.xpath('//span[contains(@class, "zknc-total-count")]/text()').get()
        else:  # error
            items['header'] = 'error scraping header'
            items['text'] = 'error scraping text'
            items['comment_count'] = 'error scraping comments'
        return items


class ZakonTestSpider(scrapy.Spider):
    name = 'zakontest'
    start_urls = [
        'https://www.zakon.kz/news'
    ]
    url = 'https://www.zakon.kz'

    def parse(self, response):

        news = response.selector.xpath('//div[contains(@class, "cat_news_item")]/a[@class]/@href').getall()
        for new in news[10:11]:
            yield SplashRequest(ZakonSpider.url + str(new), callback=self.check)

    def check(self, response):
        items = ZakonItem()
        items['href'] = response.url
        if response.selector.xpath('//div[contains(@class, "fullnews white_block")]'):
            items['header'] = response.selector.xpath('//h1/text()').get()
            text_list = response.selector.xpath('//div[contains(@id, "initial_news_story")]//p[not(self::p[@id="tg_invite"])]//text() | //div[contains(@id, "initial_news_story")]//*[contains(@class, "quote_in_news")]//text() | //div[contains(@id, "initial_news_story")]//strong//text()').getall()
            items['text'] = ''.join(elem for elem in text_list)
            items['comment_count'] = response.selector.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "zknc-total-count", " " ))]/text()').get()
        elif response.selector.xpath('//div[contains(@id, "dle-content")]'):
            items['header'] = response.selector.xpath('//span[contains(@class, "s1")]/text()').get().replace('\n', ' ').replace('\r', ' ')
            text_list = response.selector.xpath('//div[contains(@class, "WordSection")]//p[contains(@class, "MsoNormal")]/span/text()').getall()
            items['text'] = ''.join(elem.replace('\n', ' ').replace('\r', ' ') for elem in text_list)
            items['comment_count'] = response.selector.xpath('//span[contains(@class, "zknc-total-count")]/text()').get()
        return items

