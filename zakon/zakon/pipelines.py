# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

# Scraped data -> item containers -> json/csv files
# Scraped data -> item containers -> pipeline -> sql/mongo database

class ZakonPipeline(object):
    def process_item(self, item, spider):
        return item
