# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
# Extracted data -> Temporary containers (items) -> Storing in database
import scrapy


class ZakonItem(scrapy.Item):
    datetime = scrapy.Field()
    href = scrapy.Field()
    header = scrapy.Field()
    text = scrapy.Field()
    comment_count = scrapy.Field()
